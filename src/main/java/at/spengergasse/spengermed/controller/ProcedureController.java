package at.spengergasse.spengermed.controller;

import at.spengergasse.spengermed.models.Patient;
import at.spengergasse.spengermed.models.Procedure;
import at.spengergasse.spengermed.repositorys.ProcedureRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;



@RequestMapping(path = "/api/procedure")
@RestController
@CrossOrigin
public class ProcedureController {

    @Autowired
    private ProcedureRepository procedureRepository;

    @GetMapping
    public @ResponseBody
    Iterable<Procedure> getAllProcedure() {
        return procedureRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Procedure> getProcedure(@PathVariable String id) {
        return procedureRepository
                .findById(id)
                .map(procedure -> ResponseEntity.ok().body(procedure))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping()
    public ResponseEntity<Procedure> createProcedure(@Valid @RequestBody
                                                         Procedure procedure) {
        procedure.setId(null);
        var saved = procedureRepository.save(procedure);
        return ResponseEntity.created(URI.create("/api/procedure/" +
                saved.getId())).body(saved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Procedure> updateProcedure(
            @PathVariable(value = "id") String procedureId, @Valid
    @RequestBody Procedure procedureDetails) {
        return procedureRepository
                .findById(procedureId)
                .map(
                        procedure -> {

                            procedure.setIdentifier(procedureDetails.getIdentifier());
                            procedure.setPerformedAge(procedureDetails.getPerformedAge());
                            procedure.setPerformedRange(procedureDetails.getPerformedRange());
                            procedure.setStatus(procedureDetails.getStatus());
                            procedure.setStatusWhy(procedureDetails.getStatusWhy());


                            Procedure updatedProcedure =
                                    procedureRepository.save(procedure);

                            return ResponseEntity.ok(updatedProcedure);
                        })

                .orElse(createProcedure(procedureDetails));


    }

    // Delete a Patient
    @DeleteMapping("/{id}")
    public ResponseEntity<Procedure> deletePatient(@PathVariable(value = "id") String procedureId) {
        return procedureRepository.findById(procedureId).map(procedure -> {
            procedureRepository.delete(procedure);
            return ResponseEntity.ok().<Procedure>build();
        }).orElse(ResponseEntity.notFound().build());
    }

}

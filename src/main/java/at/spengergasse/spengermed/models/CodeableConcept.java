package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="cc_codableconcept")
@Builder
public class CodeableConcept extends Element{

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "c_codeableconcept_fk", referencedColumnName = "id")
    private List<Coding> coding;

    @Column(name="cc_text")
    private String text;

}

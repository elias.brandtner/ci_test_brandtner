package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Table(name="n_narrative")
@Builder
public class Narrative extends Element {

    public enum NarrativeCode {
        generated, extensions, additional, empty
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "n_status")
    private NarrativeCode status;

    @Column(name = "n_div")
    private String id;
}

package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@Builder
@Table(name="p_patient ")
public class Patient extends DomainRessource {

    @Column(name = "p_active")
    private boolean active;

    public enum GenderCode{
        male, female, other, unknown
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "p_gender")
    private GenderCode gender;

    @Column(name="p_birth")
    private LocalDate birthDate;

    @Column(name = "p_deceasedBoolean")
    private boolean deceasedBoolean;

    @Column(name = "p_deceasedDateTime")
    private LocalDate deceasedDateTime;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "i_p_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "hn_p_id", referencedColumnName = "id")
    private List<HumanName> name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "cp_p_id", referencedColumnName = "id")
    private List<ContactPoint> telecom;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "a_p_id", referencedColumnName = "id")
    private List<Address> addresses = new ArrayList<>();
}

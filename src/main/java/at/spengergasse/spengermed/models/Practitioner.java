package at.spengergasse.spengermed.models;

import lombok.*;
import org.hibernate.loader.hql.QueryLoader;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@Builder
@Table(name="pr_practitioner")
public class Practitioner extends DomainRessource {

    public enum GenderCode{
        male, female, other, unknown
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "i_pr_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    @Column(name = "pr_active")
    private boolean active;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "hn_pr_id", referencedColumnName = "id")
    private List<HumanName> name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "cp_pr_id", referencedColumnName = "id")
    private List<ContactPoint> telecom;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "a_pr_id", referencedColumnName = "id")
    private List<Address> address;

    @Enumerated(EnumType.STRING)
    @Column(name = "pr_gender")
    private Practitioner.GenderCode gender;

    @Column(name="pr_birth")
    private LocalDate birthDate;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "a_pr_photo", referencedColumnName = "id")
    private List<Attachment> photo;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "qf_pr_id", referencedColumnName = "id")
    private List<Qualification> qualification;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="cc_pr_id", referencedColumnName = "id")
    private List<CodeableConcept> communication;
}

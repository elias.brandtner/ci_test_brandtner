package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@Builder
@Table(name="po_procedure")
public class Procedure extends DomainRessource{

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "i_po_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    public enum Status{
        preparation("preparation"),
        inprogress("in-progress"),
        notdone("not-done"),
        onhold("on-hold"),
        stopped("stopped"),
        completed("completed"),
        enteredinerror("entered-in-error"),
        unknown("unknown");
        private String value;

        private Status(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "po_cc_fk", referencedColumnName = "id")
    private CodeableConcept statusWhy;

    @Column(name = "po_performedAge")
    private LocalDateTime performedAge;

    @Column(name = "po_performedRange")
    private int performedRange;


    @Enumerated(EnumType.STRING)
    @Column(name = "po_status")
    private Status status;

}

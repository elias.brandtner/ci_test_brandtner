package at.spengergasse.spengermed.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="qf_qualification")
@Builder
public class Qualification extends BackboneElement{

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "i_qf_id", referencedColumnName = "id")
    private List<Identifier> identifier;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="qf_cc_id", referencedColumnName = "id")
    private CodeableConcept code;

    @Column(name="qf_period")
    private Period period;

}

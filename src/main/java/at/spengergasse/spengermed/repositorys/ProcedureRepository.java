package at.spengergasse.spengermed.repositorys;

import at.spengergasse.spengermed.models.Procedure;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProcedureRepository extends PagingAndSortingRepository<Procedure, String> {

}

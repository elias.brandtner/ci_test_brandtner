INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('1', 'test');
INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('2', 'hello');
INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('3', 'world');

INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('22','Content 22');
INSERT INTO `spengermed`.`greeting` (`id`, `content`) VALUES ('55','Content 55');
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birth`,`p_deceased_date_time`, `p_deceased_boolean`, `p_gender`) VALUES ('asdf',1, '2000-01-01', '2010-01-01', 1, 'male');
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birth`, `p_gender`, `p_deceased_boolean`) VALUES ('gjuerighirgh', 1, '2000-01-01','unknown', 1);
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birth`,`p_gender`, `p_deceased_boolean`) VALUES ('7439re', 0, '2001-04-05','male', 0);
INSERT INTO `spengermed`.`p_patient` (`id`, `p_active`, `p_birth`,`p_gender`, `p_deceased_boolean`) VALUES ('frejifgreu89', 1, '2010-01-21','female', 1);
INSERT INTO `spengermed`.`hn_humanname` (`id`, `hn_family`,`pp_end`, `pp_start`, `hn_text`, `hn_use`, `hn_p_id`) VALUES ('1','Mustermann', '2099-12-31', '2000-01-01', 'blabla', 'usual','7439re');
INSERT INTO `spengermed`.`a_address` (`id`, `a_city`, `a_country`,`a_district`, `pp_end`, `pp_start`, `a_postalcode`, `a_state`, `a_p_id`) VALUES ('addr1', 'Orasje', 'BiH', 'Orase', '2099-12-31 16:36:34.000000', '2020-04-27 16:36:49.000000', '1111', 'BiH', '7439re');

INSERT INTO `spengermed`.`pr_practitioner` (`id`, `pr_active`, `pr_birth`,`pr_gender`) VALUES ('sjkenm983', 1, '2012-01-21','female');
INSERT INTO `spengermed`.`pr_practitioner` (`id`, `pr_active`, `pr_birth`,`pr_gender`) VALUES ('asdh43', 0, '1999-09-09','male');
INSERT INTO `spengermed`.`hn_humanname` (`id`, `hn_family`, `pp_end`, `pp_start`, `hn_text`, `hn_use`, `hn_pr_id`) VALUES ('2','Karl', '2012-11-05', '1998-05-01', 'textBasic', 'usual','sjkenm983');
INSERT INTO `spengermed`.`hn_humanname` (`id`, `hn_family`,`pp_end`, `pp_start`, `hn_text`, `hn_use`, `hn_pr_id`) VALUES ('3','Susman', '2020-03-17', '2001-03-01', 'nothing', 'usual','asdh43');
INSERT INTO `spengermed`.`a_address` (`id`, `a_city`, `a_country`,`a_district`, `pp_end`, `pp_start`, `a_postalcode`, `a_state`, `a_pr_id`) VALUES ('addrTest', 'Utopie', 'irgendwo', 'Wien', '2020-05-27 16:36:49.000000', '2016-04-27 16:36:49.000000', '1234', 'Irgendwo', 'sjkenm983');
INSERT INTO `spengermed`.`qf_qualification` (`id`, `pp_end`, `pp_start`, `qf_pr_id`) VALUES ('682345','1999-09-09', '2012-01-21', 'sjkenm983');
INSERT INTO `spengermed`.`at_attachment` (`id`, `at_content_type`, `at_creation`, `at_data`, `at_hash`, `at_language`, `at_size`, `at_title`, `at_url`, `a_pr_photo`) VALUES ('3457754','TestData', '2012-01-21', 'Lorem ipsum Datasidohfiohkjdsflhkjhdsf', '2389ud', 'german',23 , 'title', 'http:asdkjw', 'sjkenm983');

INSERT INTO `spengermed`.`cc_codableconcept` (`id`, `cc_text`, `cc_pr_id`) VALUES (1, 'ertzgh', 'sjkenm983');
INSERT INTO `spengermed`.`cc_codableconcept` (`id`, `cc_text`, `cc_pr_id`) VALUES (2, 'wertzghj', 'asdh43');
INSERT INTO `spengermed`.`cc_codableconcept` (`id`, `cc_text`) VALUES (3, 'ukgj6');
INSERT INTO `spengermed`.`cc_codableconcept` (`id`, `cc_text`) VALUES (4, 'euisdhkjfdg8');

INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_pr_id`) VALUES ('1235','official', '2012-01-21 08:30:20', '2012-01-21 12:12:12', 'systemchecksa', 'theactualValue', 'sjkenm983');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_pr_id`) VALUES ('2345','official', '2012-01-21 08:30:20', '1992-01-21 12:12:12', 'rttzu', 'sedrftghjk', 'asdh43');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_qf_id`) VALUES ('234','official', '2010-01-21 08:30:20', '1994-01-21 12:12:12', 'liute', 'ouizutz', '682345');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('6454','official', '2009-01-21 08:30:20', '1967-01-21 12:12:12', 'kzjre', 'polikuzz', 'asdf');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('3456','official', '2002-01-21 08:30:20', '1936-01-21 12:12:12', 'lizukr', 'likzujt', 'gjuerighirgh');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('8765','official', '2020-01-21 08:30:20', '1997-01-21 12:12:12', 'kuezje', 'kjezrhrw', '7439re');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_p_id`) VALUES ('24745','official', '2034-01-21 08:30:20', '1980-01-21 12:12:12', 'etzej', 'oluirzu', 'frejifgreu89');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_cc_fk`) VALUES ('245782','official', '2019-01-21 08:30:20', '1954-01-21 12:12:12', '9l846k', 'kejw', '1');
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_cc_fk`) VALUES ('03682','official', '1999-01-21 08:30:20', '1940-01-21 12:12:12', 'kjwwr5u', 'olizkuet', '2');

INSERT INTO `spengermed`.`c_coding` (`id`, `c_code`, `c_display`, `c_system`, `c_user_selected`, `c_version`, `c_codeableconcept_fk`) VALUES (1, 'asdasd', 'estrzuoip', 'erzwez', 1, 'dfj67rtez', 1);
INSERT INTO `spengermed`.`c_coding` (`id`, `c_code`, `c_display`, `c_system`, `c_user_selected`, `c_version`, `c_codeableconcept_fk`) VALUES (2, 'asfs', 'azyerdxtcgu', 'fsdgr', 1, 'jtegf', 2);

INSERT INTO `spengermed`.`cp_contactpoint` (`id`, `pp_end`, `pp_start`, `cp_rank`, `cp_system`, `cp_use`, `cp_value`, `cp_pr_id`, `cp_p_id`) VALUES (1, '1998-01-21 08:30:20', '1940-01-21 12:12:12', 125, 'phone', 'home', 'rtzfdhh', 'sjkenm983', 'asdf');

INSERT INTO `spengermed`.`pr_prefix_hn` (`id`, `prefix`) VALUES (2, 'Dr');
INSERT INTO `spengermed`.`humanname_surfix` (`id`, `surfix`) VALUES (2, 'Valule');

INSERT INTO `spengermed`.`n_narrative` (`id`, `n_status`) VALUES ("dsfjg", "additional");
INSERT INTO `spengermed`.`n_narrative` (`id`, `n_status`) VALUES ("sdfj", "empty");
INSERT INTO `spengermed`.`n_narrative` (`id`, `n_status`) VALUES ("6uhjfgh", "additional");

INSERT INTO `spengermed`.`po_procedure` (`id`, `po_performed_range`, `po_status`, `dr_n_id`, `po_cc_fk`) VALUES ("asdfggk7rtedfvcb", 12362, "unknown", "dsfjg", 3);
INSERT INTO `spengermed`.`po_procedure` (`id`, `po_performed_range`, `po_status`, `dr_n_id`, `po_cc_fk`) VALUES ("zthn456trz", 37954, "inprogress", "sdfj", 4);



INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_po_id`) VALUES ('35687122','official', '2010-01-21 08:30:20', '1940-01-21 12:12:12', 'LKWRSAHG', 'KUETZH', "asdfggk7rtedfvcb");
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_po_id`) VALUES ('8257248','official', '2005-01-21 08:30:20', '1954-01-21 12:12:12', 'LKEZTSJ', 'KETGHFS', "asdfggk7rtedfvcb");
INSERT INTO `spengermed`.`i_identifier` (`id`, `i_code`, `pp_end`, `pp_start`, `i_system`, `i_value`, `i_po_id`) VALUES ('91896292','official', '1998-01-21 08:30:20', '1940-01-21 12:12:12', 'KEZGFH', 'LITUZUK', "zthn456trz");
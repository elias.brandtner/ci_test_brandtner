package at.spengergasse.spengermed;

import at.spengergasse.spengermed.models.*;
import at.spengergasse.spengermed.repositorys.PatientRepository;
import at.spengergasse.spengermed.repositorys.PractitionerRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PractitionerRepositoryTest {

    @Autowired
    PractitionerRepository practitionerRepository;

    @Test
    @Transactional
    public void testSaveAndLoadOnePractitioner() {

        //Patientenobjekt wird erstellt
        Practitioner p = returnOnePractitioner();

        //In DB speichern
        Practitioner savedP = practitionerRepository.save(p);

        //aus DB laden
        Practitioner loadedPractitioner = practitionerRepository.findById(savedP.getId()).get();

        //vergleichen ob die Attribute gleich sind
        //4. Vergleich des gespeicherten Objekts mit dem geladenen
        //Alle einfachen Datentypen können mit Equals verglichen werden.
        //Assert prüft, ob die beiden gleich sind. Schlägt ein Assert fehl, ist der Test fehlgeschlagen
        //Asserts sind die eigentlichen "Tests"
        assertEquals(p.getBirthDate(), loadedPractitioner.getBirthDate());
        assertEquals(p.getGender(), loadedPractitioner.getGender());
        assertEquals(p.getText(), loadedPractitioner.getText());
        //Es sollen alle Attribute verglichen werden, auch die geerbten.
        //Collections werden mit CollectionUtils auf gleichheit getestet.
        // Dabei werden die einzelnen Elemente verglichen,nicht ob die Collectionobjekte gleich sind.
        assertTrue(CollectionUtils.isEqualCollection(p.getIdentifier(),
                loadedPractitioner.getIdentifier()));
        assertTrue(CollectionUtils.isEqualCollection(p.getName(),
                loadedPractitioner.getName()));
        assertTrue(CollectionUtils.isEqualCollection(p.getTelecom(),
                loadedPractitioner.getTelecom()));
        assertTrue(CollectionUtils.isEqualCollection(p.getAddress(),
                loadedPractitioner.getAddress()));
        assertTrue(CollectionUtils.isEqualCollection(p.getCommunication(),
                loadedPractitioner.getCommunication()));
        assertTrue(CollectionUtils.isEqualCollection(p.getPhoto(),
                loadedPractitioner.getPhoto()));
        assertTrue(CollectionUtils.isEqualCollection(p.getQualification(),
                loadedPractitioner.getQualification()));

        //Es sollen alle Collections getestet werden.
    }

    @Test
    @Transactional
    public void testDeleteOnePractitioner() {
        //Patientenobjekt wird erstellt
        Practitioner p = returnOnePractitioner();

        //In DB speichern
        Practitioner savedP = practitionerRepository.save(p);

        //aus DB laden
        Practitioner loadedPractitioner = practitionerRepository.findById(savedP.getId()).get();

        loadedPractitioner.setActive(false);

        Practitioner updatedP = practitionerRepository.save(loadedPractitioner);

        Practitioner loaded2Practitioner = practitionerRepository.findById(updatedP.getId()).get();


        //vergleichen ob die Attribute gleich sind
        //4. Vergleich des gespeicherten Objekts mit dem geladenen
        //Alle einfachen Datentypen können mit Equals verglichen werden.
        //Assert prüft, ob die beiden gleich sind. Schlägt ein Assert fehl, ist der Test fehlgeschlagen
        //Asserts sind die eigentlichen "Tests"
        assertEquals(updatedP.getBirthDate(), loaded2Practitioner.getBirthDate());
        assertEquals(updatedP.getGender(), loaded2Practitioner.getGender());
        assertEquals(updatedP.getText(), loaded2Practitioner.getText());
        assertEquals(updatedP.isActive(), loaded2Practitioner.isActive());
        //Es sollen alle Attribute verglichen werden, auch die geerbten.
        //Collections werden mit CollectionUtils auf gleichheit getestet.
        // Dabei werden die einzelnen Elemente verglichen,nicht ob die Collectionobjekte gleich sind.
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getIdentifier(),
                loaded2Practitioner.getIdentifier()));
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getName(),
                loaded2Practitioner.getName()));
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getTelecom(),
                loaded2Practitioner.getTelecom()));
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getAddress(),
                loaded2Practitioner.getAddress()));
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getCommunication(),
                loaded2Practitioner.getCommunication()));
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getPhoto(),
                loaded2Practitioner.getPhoto()));
        assertTrue(CollectionUtils.isEqualCollection(updatedP.getQualification(),
                loaded2Practitioner.getQualification()));

        //Es sollen alle Collections getestet werden.
    }



    @Test
    @Transactional
    public void testUpdateOnePractitioner() {
        //Patientenobjekt wird erstellt
        Practitioner p = returnOnePractitioner();

        //In DB speichern
        Practitioner savedP = practitionerRepository.save(p);

        //aus DB laden
        Practitioner loadedPractitioner = practitionerRepository.findById(savedP.getId()).get();

        //delete Patient
        practitionerRepository.delete(savedP);

        assertEquals(practitionerRepository.findById(savedP.getId()).isEmpty(), true);
    }

    public static Practitioner returnOnePractitioner() {
        List<Identifier> identifiers = new ArrayList<>();
        List<Coding> codings = new ArrayList<>();
        List<ContactPoint> contactPoints = new ArrayList<>();
        List<HumanName> names = new ArrayList<>();
        List<Address> address = new ArrayList<>();
        List<String> prefixes = null;
        List<String> suffixes = null;
        List<CodeableConcept> communication = new ArrayList<>();
        List<Attachment> photo = new ArrayList<>();
        List<Qualification> qualifications = new ArrayList<>();
        List<Identifier> qualificationsidentifiers = new ArrayList<>();


        codings.add(new Coding("System", "0.1.1", "Code", "<div>...<div>",false));
        codings.add(new Coding("System", "0.1.1", "Code2", "<div>...<div>",false));


        Period period1 = Period.builder()
                .start(LocalDateTime.of(1999, 01,01,1,1))
                .end(LocalDateTime.of(2003, 02,02,2,2))
                .build();
        Period period2 = Period.builder()
                .start(LocalDateTime.of(2000, 01,01,1,1))
                .end(LocalDateTime.of(2010, 02,02,2,2))
                .build();
        Period period3 = Period.builder()
                .start(LocalDateTime.of(2001, 01,01,1,1))
                .end(LocalDateTime.of(2011, 02,02,2,2))
                .build();
        Period period4 = Period.builder()
                .start(LocalDateTime.of(2007, 01,01,1,1))
                .end(LocalDateTime.of(2011, 02,02,2,2))
                .build();
        Period period5 = Period.builder()
                .start(LocalDateTime.of(1998, 01,01,1,1))
                .end(LocalDateTime.of(2011, 02,02,2,2))
                .build();

        CodeableConcept ccType = CodeableConcept.builder()
                .coding(codings)
                .text("<div></div>")
                .build();

        CodeableConcept ccType2 = CodeableConcept.builder()
                .coding(codings)
                .text("Codeable Concept")
                .build();

        CodeableConcept ccType3 = CodeableConcept.builder()
                .coding(codings)
                .text("Codeable Concept div")
                .build();

        CodeableConcept ccType4 = CodeableConcept.builder()
                .coding(codings)
                .text("Codeablediv </> Concept div")
                .build();

        communication.add(ccType2);


        identifiers.add(
                Identifier.builder()
                        .code(Identifier.UseCode.official)
                        .period(period1)
                        .system("System")
                        .type(ccType)
                        .value("value")
                        .build()
        );
        qualificationsidentifiers.add(
                Identifier.builder()
                        .code(Identifier.UseCode.old)
                        .period(period5)
                        .system("System")
                        .type(ccType4)
                        .value("value")
                        .build()
        );

        contactPoints.add(
                ContactPoint.builder()
                        .period(period2)
                        .rank(1)
                        .system(ContactPoint.SystemCode.email)
                        .use(ContactPoint.UseCode.home)
                        .value("pirker@spengergasse.at")
                        .build()
        );

        qualifications.add(
                Qualification.builder()
                        .period(period4)
                        .code(ccType3)
                        .identifier(qualificationsidentifiers)
                        .build()
        );

        List<String> givenNames = new ArrayList<>();
        givenNames.add("Simon");
        givenNames.add("2.Vorname");
        names.add(HumanName.builder()
                .family("Pirker")
                .given(givenNames)
                .period(Period.builder().start(LocalDateTime.now()).end(LocalDateTime.now())
                        .build())
                .use(HumanName.UseCode.anonymous)
                .build());

        address.add(
                Address.builder()
                        .city("Wien")
                        .country("Österreich")
                        .district("Wien")
                        .line("Spengergasse 20")
                        .postalcode("1050")
                        .period(period3)
                        .state("Wien")
                        .text("<div>.../</div>")
                        .type(Address.TypeCode.both)
                        .use(Address.UseCode.home)
                        .build()
        );

        photo.add(
                Attachment.builder()
                        .language("Deutsch")
                        .creation(LocalDate.of(1999, 9, 9))
                        .data("data")
                        .contentType("Photo")
                        .size(16)
                        .title("Title of photo")
                        .url("C://")
                        .hash("ZIHASIsd78687")
                        .build()
        );
        return Practitioner.builder()
                .active(true)
                .birthDate(LocalDate.of(1999, 9, 9))
                .identifier(identifiers)
                .address(address)
                .name(names)
                .telecom(contactPoints)
                .gender(Practitioner.GenderCode.male)
                .communication(communication)
                .photo(photo)
                .qualification(qualifications)
                .build();
    }
}
